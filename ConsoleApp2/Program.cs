﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Koer
    {
        public string Nimi;
        public void Haugu()
        {
            Console.WriteLine($"{Nimi} teeb Auh auh");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Koer muki = new Koer { Nimi = "Pauka" };
            muki.Haugu();
        }
    }
}
